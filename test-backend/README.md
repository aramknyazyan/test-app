## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the node server in the development mode.<br>
Open [http://localhost:4000/api](http://localhost:4000/api) to view a basic API.

You will see any errors in the console.
In dev mode you can access graphql API [http://localhost:4000/graphql](http://localhost:4000/api) web interface and run queries.

### `npm test`

Runs API tests which cover user register/authenticate and poll create/getAll/getOne cases

In order to run the server in production install PM2 NPM package globally, see the details [here](https://www.npmjs.com/package/pm2)

## API documentation

API documentation can be accessed at [http://localhost:4000/api-docs](http://localhost:4000/api-docs).
