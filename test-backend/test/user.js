const mongoose = require("mongoose");
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();

const User = require('../app/models/user');

chai.use(chaiHttp);

describe('Users', () => {
    beforeEach((done) => {
        User.deleteMany({}, (err) => {
           done();
        });
    });
    /*
    * Test the POST /api/user route
    */
    describe('/POST /api/user', () => {
        it('it should not POST a user without username and password fields', (done) => {
            const user = {};
            chai.request(server)
                .post('/api/user')
                .send(user)
                .end((err, res) => {
                      res.should.have.status(400);
                      res.body.should.have.property('err');
                      res.body.err.should.have.property('message');
                  done();
                });
        });
        it('it should POST a user and return token', (done) => {
            const user = {
                username: "testuser",
                password: "123456"
            }
            chai.request(server)
                .post('/api/user')
                .send(user)
                .end((err, res) => {
                      res.should.have.status(200);
                      res.body.should.have.property('result');
                      res.body.result.should.have.property('id');
                      res.body.result.should.have.property('token');
                  done();

                  /*
                  * Test the POST /api/authenticate route
                  */
                  describe('/POST /api/authenticate', () => {
                      it('it should POST authentication data and return token', (done) => {
                          const user = {
                              username: "testuser",
                              password: "123456"
                          }
                          chai.request(server)
                              .post('/api/authenticate')
                              .send(user)
                              .end((err, res) => {
                                    res.should.have.status(200);
                                    res.body.should.have.property('result');
                                    res.body.result.should.have.property('id');
                                    res.body.result.should.have.property('token');
                                done();
                              });
                      });
                  });
             });
        });
    });
});
