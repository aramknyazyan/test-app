const mongoose = require("mongoose");
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const should = chai.should();

const Poll = require('../app/models/poll');

chai.use(chaiHttp);
let token = '';
let userId = '';

describe('Polls', () => {
    beforeEach((done) => {
        Poll.deleteMany({}, (err) => {
          const user = {
              username: "testuser19",
              password: "123456"
          };
          if (!token) {
              chai.request(server)
                  .post('/api/user')
                  .send(user)
                  .end((err, res) => {
                      token = res.body.result.token;
                      userId = res.body.result.id;
                  done();
              });
          } else {
              done();
          }
        });
    });
    /*
    * Test the POST /api/user route without passing username and password
    */
    describe('/POST /api/poll', () => {
        it('it should POST a poll', (done) => {
            const poll = {
                title: 'testpoll',
                description: "what option to choose?",
                options: ["option1", "option2", "option3"]
            };
            chai.request(server)
                .post('/api/poll')
                .set('x-access-token', token)
                .send(poll)
                .end((err, res) => {
                      res.should.have.status(200);
                      res.body.should.have.property('result');
                      res.body.result.should.have.property('message');
                  done();
                });
        });
    });

    /*
    * Test the GET /poll/:id route
    */
    describe('/api/poll/:id poll', () => {
        it('it should GET a poll by the given id', (done) => {
            let poll = new Poll({
                title: "testpoll1",
                description: "what option to choose?",
                userId: userId,
                options: [{"name": "option1", value: 0},{"name": "option2", value: 0}]
            });
            poll.save((err, newPoll) => {
                chai.request(server)
                    .get('/api/poll/' + newPoll._id)
                    .end((err, res) => {
                          res.should.have.status(200);
                          res.body.should.have.property('result');
                          res.body.result.should.have.property('poll');
                          res.body.result.poll.should.have.property('_id').eql(newPoll._id);
                          res.body.result.poll.should.have.property('title').eql('testpoll1');
                      done();
                    });
            });
        });
    });
    /*
    * Test the GET /poll route
    */
    describe('/api/poll poll', () => {
        it('it should GET a list of all polls', (done) => {
            let poll = new Poll({
                title: "testpoll1",
                description: "what option to choose?",
                userId: userId,
                options: [{"name": "option1", value: 0},{"name": "option2", value: 0}]
            });
            poll.save((err, newPoll) => {
                chai.request(server)
                    .get('/api/poll')
                    .end((err, res) => {
                          res.should.have.status(200);
                          res.body.should.have.property('result');
                          res.body.result.should.have.property('polls');
                          res.body.result.polls.should.be.a('array');
                          res.body.result.polls[0].should.have.property('_id').eql(newPoll._id);
                          res.body.result.polls[0].should.have.property('title').eql('testpoll1');
                      done();
                    });
            });
        });
    });
});
