const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const config = require('./app/config/config');
const swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

// use body parser so we can grab information from POST requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// configure our app to handle CORS requests
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS, HEAD');
    res.setHeader("Access-Control-Allow-Headers", "X-access-token, x-access-token, Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    if (req.method === 'OPTIONS') {
        return res.end();
    }
    next();
});

let env = process.env.NODE_ENV || 'dev';

// log all requests to the console
if (env === 'dev') {
    app.use(morgan('dev'));
}

// API ROUTES
const api = require('./app/routes/api')(app, express);
app.use('/api', api);

// API DOCS
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// if API ROUTE not found return invalid url error
app.get('*', (req, res) => {
    res.redirect('/api/error');
});

// START THE SERVER
app.listen(config.port);
console.log('Node server started on port ' + config.port);

module.exports = app; // for testing
