const User = require('../models/user');

class UserRepository {

    static createUser(userObj, callback) {
        let user = new User(userObj);
        user.save((err, newUser) => {
            if (err) {
                if (err.code === 11000) {
                    return callback({
                        success: false,
                        code: 400,
                        err: {
                            message: `Failed to save user: an user with username: '${userObj.username}' already exists!`
                        }
                    })
                }
                return callback({
                    success: false,
                    code: 500,
                    err: {
                        message: `Failed to save user: ${userObj}, err: ${JSON.stringify(err)}!`
                    }
                })
            }
            return callback({success: true, code: 200, result: {user: newUser}});
        })
    }

    static getOneUser(userId, callback) {
        User.findById(userId, (err, user) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }
            if (!user) {
                return callback({success: false, code: 400, err: {message: `Failed to find user with id: ${userId}!`}});
            }
            return callback({success: true, code: 200, result: {user: user}});
        })
    }

    static updateUser(req, callback) {
        User.findById(
            req.params.id,
            (err, user) => {
                if (err) {
                    return callback({success: false, code: 500, err: err});
                }
                if (!user) {
                    return callback({
                        success: false,
                        code: 403,
                        err: {
                            message: `Failed to update user: there is no user with id: ${req.params.id} !`
                        }
                    })
                }
                if (req.body.username) {
                    user.username = req.body.username;
                }
                if (req.body.password) {
                    user.password = req.body.password;
                }
                user.save(err => {
                    if (err) {
                        if (err.code === 11000) {
                            return callback({
                                success: false,
                                code: 500,
                                err: {
                                    message: `Failed to update user: an user with username: '${req.body.username}' already exists !`
                                }
                            })
                        }
                        return callback({success: false, code: 500, err: err});
                    }
                    return callback({success: true, code: 200, result: {message: 'OK'}});
                })
            }
        )
    }

    static deleteUser(userId, callback) {
        User.findOneAndRemove(
            {_id: userId},
            (err, user) => {
                if (err) {
                    return callback({success: false, code: 500, err: err});
                }
                if (!user) {
                    return callback({
                        success: false,
                        code: 400,
                        err: {
                            message: `Failed to remove user with id: ${userId}, user not found !`
                        }
                    })
                }
                return callback({success: true, code: 200, result: {message: 'OK'}});
            }
        )
    }

    static authenticate(req, callback) {
        User.findOne({
            username: req.body.username
        }).select('username password').exec((err, user) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }
            if (!user) {
                return callback({
                    success: false,
                    code: 403,
                    err: {
                        message: `Failed to find user with username: '${req.body.username}' !`
                    }
                })
            }
            if (!user.comparePassword(req.body.password)) {
                return callback({
                    success: false,
                    code: 400,
                    err: {
                        message: `Failed to authenticate user with username: '${req.body.username}', wrong password !`
                    }
                })
            }
            return callback({success: true, code: 200, result: {message: 'OK', user: user}});
        })
    }
}

module.exports = UserRepository;
