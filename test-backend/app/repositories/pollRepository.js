const Poll = require('../models/poll');

class PollRepository {

    static createPoll(pollEntity, callback) {
        let poll = new Poll(pollEntity);
        poll.save((err, newPoll) => {
            if (err) {
                if (err.code === 11000) {
                    return callback({ success: false, code: 409, err: { message: `Failed to save poll, a poll with title: '${pollEntity.title}' already exists!`} });
                }
                return callback({success: false, code: 500, err: err });
            }
            return callback({success: true, code: 200, result: {poll: newPoll}});
        });
    }

    static updatePoll(pollId, optionName, callback) {
        Poll.update({_id: pollId, 'options.name': optionName}, {$inc: {'options.$.value': 1}}, (err, response) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }
            return callback({success: true, code: 200, result: {message: "Successfully voted!"}});
        })
    }

    static getAllPolls(query, callback) {
        Poll.find(query, (err, polls) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }
            return callback({success: true, code: 200, result: {polls: polls}});
        });
    }

    static getOnePoll(pollId, callback) {
        Poll.findById(pollId, (err, poll) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }
            if (!poll) {
                return callback({success: false, code: 404, err: {message: `Failed to find poll with id: ${pollId}!`}});
            }
            return callback({success: true, code: 200, result: {poll: poll}});
        });
    }

    static deletePoll(query, callback) {
        Poll.findOneAndRemove(query, (err, poll) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }
            if (!poll) {
                return callback({success: false, code: 404, err: {message: `Failed to remove poll!`} });
            }
            return callback({success: true, code: 200, result: {message: 'OK', poll: poll}});
        });
    }
}

module.exports = PollRepository;
