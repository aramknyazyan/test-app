const Vote = require('../models/vote');

class VoteRepository {

    static createVote(voteEntity, callback) {
        let query = {userId: voteEntity.userId, pollId: voteEntity.pollId};
        Vote.findOne(query, (err, voteData) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }
            if (voteData) {
                return callback({success: false, code: 409, err: {message: "You have already voted for this poll!"}});
            }

            let vote = new Vote(voteEntity);
            vote.save((err, newVote) => {
                if (err) {
                    return callback({success: false, code: 500, err: err});
                }
                return callback({success: true, code: 200, result: {vote: newVote}});
            });
        })
    }

    static getUserVote(query, callback) {
        Vote.findOne(query, (err, vote) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }
            if (!vote) {
                return callback({success: false, code: 404, err: {message: "Vote not found!"}});
            }

            return callback({success: true, code: 200, result: {vote: vote}});
        })
    }

    static deleteVote(query, callback) {
        Vote.remove(query, (err, votes) => {
            if (err) {
                return callback({success: false, code: 500, err: err});
            }

            return callback({success: true, code: 200, result: {message: "OK"}});
        })
    }
}

module.exports = VoteRepository;
