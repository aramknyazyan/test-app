const config = require('../config/config');
const parameters = require('../config/parameters');
const PollValidator = require('../validators/pollValidator');
const PollRepository = require('../repositories/pollRepository');
const VoteRepository = require('../repositories/voteRepository');

class PollService {
    static createPoll(req, callback) {
        const validationResult = PollValidator.createPoll(req);
        if (!validationResult.success) {
            return callback(validationResult);
        }
        let options = [];
        req.body.options.forEach(function(option, index){
            options.push({name: req.body.options[index], value: 0});
        })
        let poll = {
            title: req.body.title,
            description: req.body.description,
            options: options,
            userId: req.decoded.id
        };
        PollRepository.createPoll(poll, response => {
            if (!response.success) {
                return callback(response);
            }
            const newPoll = response.result.poll;
            return callback({success: true, code: 200, result: {message: 'OK', pollId: newPoll._id}});
        });
    }

    static getAllPolls(req, callback) {

        const validationResult = PollValidator.getAllPolls(req);
        if (!validationResult.success) {
            return callback(validationResult);
        }
        let query = (req.query.userId) ? {userId: Number(req.query.userId)} : {};

        PollRepository.getAllPolls(query, response => {
            if (!response.success) {
                return callback(response);
            }

            return callback({success: true, code: 200, result: {message: 'OK', polls: response.result.polls}});
        });
    }

    static getOnePoll(req, callback) {
        const validationResult = PollValidator.getOnePoll(req);
        if (!validationResult.success) {
            return callback(validationResult);
        }
        PollRepository.getOnePoll(req.params.id, response => {
            if (!response.success) {
                return callback(response);
            }

            const poll = response.result.poll;
            return callback({success: true, code: 200, result: {message: 'OK', poll: poll}});
        })
    }

    static deletePoll(req, callback) {
        const validationResult = PollValidator.deletePoll(req);
        if (!validationResult.success) {
            return callback(validationResult);
        }
        PollRepository.deletePoll({_id: req.params.id, userId: req.decoded.id}, response => {
            if (!response.success) {
                return callback(response);
            }
            VoteRepository.deleteVote({pollId: req.params.id}, voteResponse => {
                if (!voteResponse.success) {
                    return callback(voteResponse);
                }
                return callback({success: true, code: 200, result: {message: 'OK'}})
            });
        })
    }
}

module.exports = PollService;
