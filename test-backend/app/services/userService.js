const jwt = require('jsonwebtoken');
const config = require('../config/config');
const parameters = require('../config/parameters');
const UserValidator = require('../validators/userValidator');
const UserRepository = require('../repositories/userRepository');

class UserService {
    static createUser(req, callback) {
        const validationResult = UserValidator.createUser(req);
        if (!validationResult.success) {
            return callback(validationResult);
        }

        let userData = {
            username: req.body.username,
            password: req.body.password
        };

        UserRepository.createUser(userData, response => {
            if (!response.success) {
                return callback(response);
            }
            const user = response.result.user;
            const superSecret = config.secret;
            const expiration = parameters.tokenExpiration;
            const token = jwt.sign({
                id: user._id,
                username: user.username
            }, superSecret, {
                expiresIn: expiration+'m'
            });
            return callback({
                success: true,
                code: 200,
                result: {
                    id: user._id,
                    username: user.username,
                    token: token,
                    message: 'Enjoy your token!',
                    exp: (Date.now() / 1000) + (expiration * 60)
                }
            });
        });
    }

    static authenticate(req, callback) {
        const validationResult = UserValidator.authenticate(req);
        if (!validationResult.success) {
            return callback(validationResult);
        }
        UserRepository.authenticate(req, response => {
            if (!response.success) {
                return callback(response);
            }
            const user = response.result.user;
            const superSecret = config.secret;
            const expiration = parameters.tokenExpiration;
            const token = jwt.sign({
                id: user._id,
                username: user.username
            }, superSecret, {
                expiresIn: expiration+'m'
            });
            return callback({
                success: true,
                code: 200,
                result: {
                    id: user._id,
                    username: user.username,
                    token: token,
                    message: 'Enjoy your token!',
                    exp: (Date.now() / 1000) + (expiration * 60)
                }
            })
        })
    }
}

module.exports = UserService;
