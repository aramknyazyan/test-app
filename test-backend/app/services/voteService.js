const config = require('../config/config');
const parameters = require('../config/parameters');
const VoteValidator = require('../validators/voteValidator');
const VoteRepository = require('../repositories/voteRepository');
const PollRepository = require('../repositories/pollRepository');

class VoteService {
    static createVote(req, callback) {

        const validationResult = VoteValidator.createVote(req);
        if (!validationResult.success) {
            return callback(validationResult);
        }
        //authorization check
        if (Number(req.decoded.id) !== Number(req.body.userId)) {
            return callback({success: false, code: 403, err: {message: "Unauthorized, permission denied!"}});
        }
        PollRepository.getOnePoll(req.body.pollId, pollResponse => {
            if (!pollResponse.success) {
                return callback(pollResponse);
            }
            let vote = {
                userId: req.body.userId,
                pollId: req.body.pollId,
                optionName: req.body.optionName
            };

            VoteRepository.createVote(vote, response => {
                if (!response.success) {
                    return callback(response);
                }
                const newVote = response.result.vote;
                PollRepository.updatePoll(req.body.pollId, req.body.optionName, pollUpdateResponse => {
                    if (!pollUpdateResponse.success) {
                        return callback(pollUpdateResponse);
                    }
                    return callback(response);
                });
            });
        });
    }

    static getUserVote(req, callback) {

        const validationResult = VoteValidator.getUserVote(req);
        if (!validationResult.success) {
            return callback(validationResult);
        }
        //authorization check
        if (Number(req.decoded.id) !== Number(req.params.userId)) {
            return callback({success: false, code: 403, err: {message: "Unauthorized, permission denied!"}});
        }
        let query = {userId: req.params.userId, pollId: req.params.pollId};
        VoteRepository.getUserVote(query, response => {
            return callback(response);
        });
    }
}

module.exports = VoteService;
