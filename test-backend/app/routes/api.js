const jwt = require('jsonwebtoken');
const graphqlHTTP = require('express-graphql');
const { buildSchema, GraphQLObjectType, GraphQLString } = require('graphql');
const Resolver = require('../graphql/resolver');
const Schema = require('../graphql/schema');
const UserController = require('../controllers/userController');
const PollController = require('../controllers/pollController');
const VoteController = require('../controllers/voteController');
const config = require('../config/config');

module.exports = (app, express) => {
    const apiRouter = express.Router();

    let isDev = (process.env.NODE_ENV === 'dev');
    // GraphQL API
    app.use('/graphql', graphqlHTTP({
        schema: Schema,
        rootValue: Resolver,
        graphiql: isDev,
    }));

    /**
     * ROOT ROUTE
     */
    apiRouter.get('/', (req, res) => {
        res.status(200).send({success: true, code: 200, result: {message:'public api'}});
    });

    /**
     * ERROR ROUTE
     */
    apiRouter.get('/error', (req, res) => {
        res.status(404).send({success: false, code: 404, err: {message:'Invalid URL'}});
    });

    // User actions
    apiRouter.post('/user', UserController.createUser);
    apiRouter.post('/authenticate', UserController.authenticate);

    // Poll actions
    apiRouter.get('/poll', PollController.getAllPolls);
    apiRouter.get('/poll/:id', PollController.getOnePoll);

    /**
     * Middleware to verify a token
     */
    apiRouter.use(function(req, res, next) {

         // check header or url parameters or post parameters for token
         let token = req.headers['x-access-token'] || req.body.token || req.query.token;

         // decode token
         if (!token) {
             res.status(403).send({ success: false, code: 403, err: { message: 'No token provided!'} });
         } else {
             // verifies secret and checks expiration
             jwt.verify(token, config.secret, function(err, decoded) {
                 if (err) {
                     res.status(403).send({ success: false, code: 403, err: { message: 'Failed to authenticate token '+ token } });
                 } else {
                     decoded.token = token;
                     req.decoded = decoded;
                     next();
                 }
             });
         }
    });

    // User actions
    //apiRouter.patch('/user/:id', UserController.updateUser);
    //apiRouter.get('/user', UserController.getAllUsers);
    //apiRouter.get('/user/:id', UserController.getOneUser);
    //apiRouter.delete('/user/:id', UserController.deleteUser);
    apiRouter.get('/user/:userId/poll/:pollId/vote', UserController.getUserVote);

    // Poll actions
    apiRouter.post('/poll', PollController.createPoll);
    apiRouter.delete('/poll/:id', PollController.deletePoll);

    // Vote actions
    apiRouter.post('/vote', VoteController.createVote);

    return apiRouter;
};
