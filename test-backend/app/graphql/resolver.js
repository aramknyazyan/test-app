const config = require('../config/config');
const fetch = require('node-fetch');
const bluebird = require('bluebird');
fetch.Promise = bluebird;

const apiUrl = config.apiUrl;
const Resolver = {
    hello: () => {
      return 'Hello world!';
    },
    poll: (args) => {
      return fetch(`${apiUrl}/poll/${args.id}`)
        .then(res => res.json())
        .then(json => json.result.poll)
    },
    polls: () => {
      return fetch(`${apiUrl}/poll`)
        .then(res => res.json())
        .then(json => json.result.polls)
    },
};

module.exports = Resolver;
