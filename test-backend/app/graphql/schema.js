const { buildSchema, GraphQLSchema, GraphQLObjectType, GraphQLString, GraphQLList } = require('graphql');

// Construct a schema, using GraphQL schema language
const Schema = buildSchema(`
    type Query {
      hello: String,
      poll(id: ID!): Poll,
      polls: [Poll!]!
    }
    type Poll {
      _id: ID!
      title: String!
      description: String!
      userId: String!
      options: [PollOption]!
    }
    type PollOption {
      name: String!
      value: Int!
    }
`);

const PollType = new GraphQLObjectType({
  name: "Poll",
  fields: () => ({
      id: { type: GraphQLString },
      title: { type: GraphQLString },
      description: { type: GraphQLString },
      userId: { type: GraphQLString },
      options: { type: new GraphQLList(GraphQLObjectType) }
  })
});

module.exports = Schema;
