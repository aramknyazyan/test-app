const validator = require('validator');

class HelperValidator {
    static validateId(id) {
        if (id === undefined) {
            return {success: false, code: 400, err: {message: `Missing parameter 'id'!`}};
        }
        if (!validator.isNumeric(String(id))) {
            return ({
                success: false,
                code: 400,
                err: {message: "Parameter 'id' must be numeric!", key: "invalid_id"}
            });
        }
        return {success: true, code: 200, result: {message: 'OK'}};
    }
}

module.exports = HelperValidator;
