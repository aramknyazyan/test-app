const validator = require('validator');
const HelperValidator = require('./helperValidator');
const parameters = require('../config/parameters');

class PollValidator {
    static createPoll(req) {
        if (req.body.title === undefined) {
            return {success: false, code: 400, err: {message: `Missing parameter 'title'!`}};
        }
        if (req.body.description === undefined) {
            return {success: false, code: 400, err: {message: `Missing parameter 'description'!`}};
        }
        if (req.body.options === undefined) {
            return {success: false, code: 400, err: {message: `Missing parameter 'options'!`}};
        }
        if (!(validator.isLength(req.body.title, {min: parameters.nameMin, max: parameters.nameMax}))) {
            return {success: false, code: 400, err: {message: `Title should be of length [${parameters.nameMin}, ${parameters.nameMax}]!`}};
        }
        if (!(validator.isLength(req.body.description, {min: parameters.textMin, max: parameters.textMax}))) {
            return {success: false, code: 400, err: {message: `Description should be of length [${parameters.textMin}, ${parameters.textMax}]!`}};
        }
        if (!(req.body.options instanceof Array)) {
            return {success: false, code: 400, err: {message: `Invalid options`}};
        }

        let options = req.body.options;
        let optionsLength = options.length;
        for (let i=0; i<optionsLength; i++) {
            if (!(validator.isLength(options[i], {min: parameters.optionMin, max: parameters.optionMax}))) {
                return {success: false, code: 400, err: {message: `Option should be of length [${parameters.optionMin}, ${parameters.optionMax}]!`}};
            }
        }

        return {success: true, code: 200, result: {message: 'OK'}};
    }

    static getAllPolls(req) {
        if (req.query.userId) {
            return HelperValidator.validateId(req.query.userId);
        }
        return {success: true, code: 200, result: {message: 'OK'}};
    }

    static getOnePoll(req) {
        return HelperValidator.validateId(req.params.id);
    }

    static deletePoll(req) {
        return HelperValidator.validateId(req.params.id);
    }
}

module.exports = PollValidator;
