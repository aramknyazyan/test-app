const validator = require('validator');
const HelperValidator = require('./helperValidator');
const parameters = require('../config/parameters');

class VoteValidator {
    static createVote(req) {
        let validateUserId = HelperValidator.validateId(req.body.userId);
        if (!validateUserId.success) {
            return {success: false, code: 400, err: {message: `Missing or invalid parameter 'userId'!`}};
        }
        let validatePollId = HelperValidator.validateId(req.body.pollId);
        if (!validatePollId.success) {
            return {success: false, code: 400, err: {message: `Missing or invalid parameter 'pollId'!`}};
        }
        if (req.body.optionName === undefined) {
            return {success: false, code: 400, err: {message: `Missing or invalid parameter 'optionName'!`}};
        }
        if (!(validator.isLength(req.body.optionName, {min: parameters.optionMin, max: parameters.optionMax}))) {
            return {success: false, code: 400, err: {message: `OptionName should be of length [${parameters.optionMin}, ${parameters.optionMax}]!`}};
        }

        return {success: true, code: 200, result: {message: 'OK'}};
    }

    static getUserVote(req) {
        let validateUserId = HelperValidator.validateId(req.params.userId);
        if (!validateUserId.success) {
            return {success: false, code: 400, err: {message: `Missing or invalid parameter 'userId'!`}};
        }
        let validatePollId = HelperValidator.validateId(req.params.pollId);
        if (!validatePollId.success) {
            return {success: false, code: 400, err: {message: `Missing or invalid parameter 'pollId'!`}};
        }

        return {success: true, code: 200, result: {message: 'OK'}};
    }
}

module.exports = VoteValidator;
