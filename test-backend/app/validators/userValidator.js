const validator = require('validator');
const parameters = require('../config/parameters');

class UserValidator {
    static createUser(req) {
        if (req.body.username === undefined) {
            return {success: false, code: 400, err: {message: `Missing parameter 'username'!`}};
        }
        if (req.body.password === undefined) {
            return {success: false, code: 400, err: {message: `Missing parameter 'password'!`}};
        }
        if (!(validator.isLength(req.body.username, {min: parameters.nameMin, max: parameters.nameMax}))) {
            return {success: false, code: 400, err: {message: `Username should be of length [${parameters.nameMin}, ${parameters.nameMax}]!`}};
        }
        if (!(validator.isLength(req.body.password, {min: parameters.passwordMin, max: parameters.passwordMax}))) {
            return {success: false, code: 400, err: {message: `Password should be of length [${parameters.passwordMin}, ${parameters.passwordMin}]!`}};
        }

        return {success: true, code: 200, result: {message: 'OK'}};
    }

    static authenticate(req) {
        if (req.body.username === undefined) {
            return {success: false, code: 400, err: {message: `Missing parameter 'username'!`}};
        }
        if (req.body.password === undefined) {
            return {success: false, code: 400, err: {message: `Missing parameter 'password'!`}};
        }
        if (!(validator.isLength(req.body.username, {min: parameters.nameMin, max: parameters.nameMax}))) {
            return {success: false, code: 400, err: {message: `Username should be of length [${parameters.nameMin}, ${parameters.nameMax}]!`}};
        }
        if (!(validator.isLength(req.body.password, {min: parameters.passwordMin, max: parameters.passwordMax}))) {
            return {success: false, code: 400, err: {message: `Password should be of length [${parameters.passwordMin}, ${parameters.passwordMin}]!`}};
        }

        return {success: true, code: 200, result: {message: 'OK'}};
    }
}

module.exports = UserValidator;
