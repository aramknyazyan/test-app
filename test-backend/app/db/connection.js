const config   = require('../config/config');
const mongoose = require('mongoose');

const dbConnection = mongoose.createConnection(config.dbUri, config.dbOptions);
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

module.exports = dbConnection;
