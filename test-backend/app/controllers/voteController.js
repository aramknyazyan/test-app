const VoteService = require('../services/voteService');

class VoteController {

    static createVote(req, res) {
        VoteService.createVote(req, result => {
            return res.status(result.code).send(result);
        });
    }
}

module.exports = VoteController;
