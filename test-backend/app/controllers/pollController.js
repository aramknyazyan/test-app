const PollService = require('../services/pollService');

class PollController {

    static createPoll(req, res) {
        PollService.createPoll(req, result => {
            return res.status(result.code).send(result);
        });
    }

    static getAllPolls(req, res) {
        PollService.getAllPolls(req, result => {
            return res.status(result.code).send(result);
        })
    }

    static getOnePoll(req, res) {
        PollService.getOnePoll(req, result => {
            return res.status(result.code).send(result);
        })
    }

    static deletePoll(req, res) {
        PollService.deletePoll(req, result => {
            return res.status(result.code).send(result);
        })
    }
}

module.exports = PollController;
