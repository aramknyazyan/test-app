const UserService = require('../services/userService');
const VoteService = require('../services/voteService');

class UserController {

    static createUser(req, res) {
        UserService.createUser(req, result => {
            return res.status(result.code).send(result);
        });
    }

    static authenticate(req, res) {
        UserService.authenticate(req, result => {
            return res.status(result.code).send(result);
        })
    }

    static getUserVote(req, res) {
        VoteService.getUserVote(req, result => {
            return res.status(result.code).send(result);
        })
    }
}

module.exports = UserController;
