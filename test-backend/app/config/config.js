let dbName = "test-backend";
if (process.env.NODE_ENV === "test") {
    dbName = "test-backend-testing";
}

module.exports = {
    port: 4000,
    secret: 'Lkdz198lG6a0OJNU0mEY',
    dbUri: 'mongodb://mongo:27017/' + dbName,
    dbOptions: {useNewUrlParser: true},
    apiUrl: "http://localhost:4000/api"
};
