module.exports = {
    nameMin: 3,
    nameMax: 24,
    passwordMin: 6,
    passwordMax: 16,
    textMin: 1,
    textMax: 60,
    optionMin: 1,
    optionMax: 60,
    tokenExpiration: 1440 //expires in 24 hours
};
