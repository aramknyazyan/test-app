const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const connection = require('../db/connection');
const parameters = require('../config/parameters');
const Schema = mongoose.Schema;
autoIncrement.initialize(connection);

const VoteSchema = new Schema({
        userId: { type: Number, required: true },
        pollId: { type: Number, required: true },
        optionName: { type: String, required: true },
        dateCreated: { type: Date, default: Date.now }
    },
    {
        versionKey: false
    }
);

VoteSchema.plugin(autoIncrement.plugin, { model: 'Vote', startAt: 1 });
module.exports = connection.model('Vote', VoteSchema);
