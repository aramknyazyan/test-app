const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const connection = require('../db/connection');
const parameters = require('../config/parameters');
const Schema = mongoose.Schema;
autoIncrement.initialize(connection);

const PollSchema = new Schema({
        title: { type: String, required: true, index: {unique: true} },
        description: { type: String, default: null },
        options: { type: Array },
        userId: {type: Number, required: true, index: true},
        dateCreated: { type: Date, default: Date.now }
    },
    {
        versionKey: false
    }
);

PollSchema.plugin(autoIncrement.plugin, { model: 'Poll', startAt: 1 });
module.exports = connection.model('Poll', PollSchema);
