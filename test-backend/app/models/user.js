const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const bcrypt = require('bcrypt-nodejs');
const connection = require('../db/connection');
const parameters = require('../config/parameters');
const Schema = mongoose.Schema;

autoIncrement.initialize(connection);

const UserSchema = new Schema({
        username: { type: String, required: true, index: {unique: true} },
        password: { type: String, default: null, select: false },
        dateCreated: { type: Date, default: Date.now, index: true }
    },
    {
        versionKey: false
    }
);

// hash the password before the user is saved
UserSchema.pre('save', function(next) {
    bcrypt.hash(this.password, null, null, (err, hash) => {
        if (err) {
            return next(err);
        }
        this.password = hash;
        next();
    })
});

// method to compare a given password with the database hash
UserSchema.methods.comparePassword = function(password) {
    const user = this;
    return bcrypt.compareSync(password, user.password);
};

UserSchema.plugin(autoIncrement.plugin, { model: 'User', startAt: 1 });
module.exports = connection.model('User', UserSchema);
