import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import HeaderContainer from './containers/HeaderContainer';
import HomeContainer from './containers/HomeContainer';
import NewPollContainer from './containers/NewPollContainer';
import PollsContainer from './containers/PollsContainer';
import PollContainer from './containers/PollContainer';
import LoginContainer from './containers/LoginContainer';
import RegisterContainer from './containers/RegisterContainer';

function Routes() {
  return (
    <Router>
      <div>
        <HeaderContainer/>
        <Route exact path="/" component={HomeContainer} />
        <Route path="/newPoll" component={NewPollContainer} />
        <Route path="/polls" component={PollsContainer} />
        <Route path="/poll/:id" component={PollContainer} />
        <Route path="/login" component={LoginContainer} />
        <Route path="/register" component={RegisterContainer} />
      </div>
    </Router>
  );
}

export default Routes
