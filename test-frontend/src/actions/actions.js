import axios from 'axios';
import setToken from '../setToken';
import { GET_VALIDATION_ERRORS, GET_ERRORS, SET_CURRENT_USER, SET_POLL, SET_POLLS, SET_MY_POLLS, SET_VOTE, SET_POLL_DELETED } from './types';
import { apiUrl } from '../configs/config.json';
import ValidationService from '../validation/ValidationService';

export const register = (user, history) => dispatch => {
    let validation = ValidationService.validateRegistration(user);
    if (!validation.success) {
        return dispatch({
            type: GET_VALIDATION_ERRORS,
            payload: validation.errors
        });
    }
    axios.post(apiUrl + '/user', user)
        .then(res => {
            const { token } = res.data.result;
            const { username } = res.data.result;
            const { id } = res.data.result;
            const { exp } = res.data.result;
            localStorage.setItem('token', token);
            localStorage.setItem('username', username);
            localStorage.setItem('userId', id);
            localStorage.setItem('exp', exp);
            setToken(token);
            dispatch(setCurrentUser({userId: id, username: username}));
            history.push('/');
        })
        .catch(err => {
            console.log(err.response.data);
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
}

export const login = (user, history) => dispatch => {
    let validation = ValidationService.validateAuthentication(user);
    if (!validation.success) {
        return dispatch({
            type: GET_VALIDATION_ERRORS,
            payload: validation.errors
        });
    }
    axios.post(apiUrl + '/authenticate', user)
        .then(res => {
            console.log(res.data.result);
            const { token } = res.data.result;
            const { username } = res.data.result;
            const { id } = res.data.result;
            const { exp } = res.data.result;
            localStorage.setItem('token', token);
            localStorage.setItem('username', username);
            localStorage.setItem('userId', id);
            localStorage.setItem('exp', exp);
            setToken(token);
            dispatch(setCurrentUser({userId: id, username: username}));
            history.push('/');
        })
        .catch(err => {
            console.log(err.response.data);
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
}

export const logout = (history) => dispatch => {
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('userId');
    localStorage.removeItem('exp');
    setToken(false);
    dispatch(setCurrentUser({}));
    if (history) {
        history.push('/login');
    }
}

export const setCurrentUser = user => {
    return {
        type: SET_CURRENT_USER,
        payload: user
    }
}

export const createPoll = (poll, history) => dispatch => {
    let validation = ValidationService.validateCreatePoll(poll);
    if (!validation.success) {
        return dispatch({
            type: GET_VALIDATION_ERRORS,
            payload: validation.errors
        });
    }
    axios.post(apiUrl + '/poll', poll)
        .then(res => {
            console.log(res);
            history.push('/poll/' + res.data.result.pollId);
        })
        .catch(err => {
            console.log(err.response.data);
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
}

export const getOnePoll = pollId => dispatch => {
    axios.get(apiUrl + '/poll/' + pollId)
        .then(res => {
            console.log(res.data.result);
            dispatch({
                type: SET_POLL,
                payload: res.data.result.poll
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
}

export const getAllPolls = (params) => dispatch => {
    let url = apiUrl + '/poll';
    if (params.userId) {
        url += '?userId=' + params.userId;
    }
    axios.get(url)
        .then(res => {
            const action = params.userId ? SET_MY_POLLS : SET_POLLS;
            dispatch({
                type: action,
                payload: res.data.result.polls
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
}

export const deletePoll = pollId => dispatch => {
    axios.delete(apiUrl + '/poll/' + pollId)
        .then(res => {
            console.log(res.data.result);
            dispatch({
                type: SET_POLL_DELETED,
                payload: "Poll is successfully deleted!"
            });
        })
        .catch(err => {
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
}

export const createVote = (vote, history) => dispatch => {
    vote.userId = localStorage.userId;
    axios.post(apiUrl + '/vote', vote)
        .then(res => {
            console.log(res);
            dispatch(setVote(res.data.result.vote));
        })
        .catch(err => {
            console.log(err.response.data);
            dispatch({
                type: GET_ERRORS,
                payload: err.response.data
            });
        });
}

export const getUserVote = (userId, pollId) => dispatch => {
    axios.get(apiUrl + `/user/${userId}/poll/${pollId}/vote`)
        .then(res => {
            console.log(res);
            dispatch(setVote(res.data.result.vote));
        })
        .catch(err => {
            console.log(err.response.data);
            if (err.response.data.code === 404) {
                dispatch(setVote({}));
            } else {
                dispatch({
                    type: GET_ERRORS,
                    payload: err.response.data
                });
            }
        });
}

export const setVote = vote => {
    return {
        type: SET_VOTE,
        payload: vote
    }
}
