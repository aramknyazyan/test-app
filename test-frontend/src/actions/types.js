export const GET_VALIDATION_ERRORS = 'GET_VALIDATION_ERRORS';
export const GET_ERRORS = 'GET_ERRORS';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const SET_POLL = 'SET_POLL';
export const SET_POLLS = 'SET_POLLS';
export const SET_MY_POLLS = 'SET_MY_POLLS';
export const SET_VOTE = 'SET_VOTE';
export const SET_POLL_DELETED = 'SET_POLL_DELETED';
