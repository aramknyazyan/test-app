import React, { Component } from "react";
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/css/style.css';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {}
        };
    }

    render() {
        return (
            <div>
                <div className="container content">
                    <div className="alert alert-info text-center">
                        Welcome to Polling application. View/Create polls and Vote.
                    </div>
                </div>
            </div>
        );
    }
}

Home.propTypes = {
    user: PropTypes.object.isRequired
}

export default Home;
