import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/css/style.css';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user
        };
        this.handleLogout = this.handleLogout.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.user !== nextProps.user) {
            this.setState({user: nextProps.user});
        }
    }

    handleLogout() {
        this.props.logout(this.props.history);
    }

    render() {
        const {user} = this.state;
        return (
            <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
                <div className="container">
                    <Link className="navbar-brand" to="/">Polling App</Link>
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link className="nav-link" to="/">Home</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/polls">Polls</Link>
                        </li>
                        <li className="nav-item">
                            {user.username ?
                                <span className="nav-link" onClick={this.handleLogout}>Logout</span>
                            :
                                <Link className="nav-link" to="/login">Login</Link>
                            }
                        </li>
                        {user.username ?
                            ""
                        :
                            <li className="nav-item">
                                <Link className="nav-link" to="/register">Register</Link>
                            </li>
                        }
                    </ul>
                </div>
            </nav>
        );
    }
}

Header.propTypes = {
    logout: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired
}

export default withRouter(Header);
