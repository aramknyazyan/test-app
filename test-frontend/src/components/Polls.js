import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';

class Polls extends Component {
    constructor(props) {
        super(props);
        this.state = {
            polls: [],
            myPolls: [],
            showMyPolls: "hide",
            pollDeleted: '',
            user: {},
            errors: {}
        };
        this.handleDeletePoll = this.handleDeletePoll.bind(this);
        this.handleSetPollType = this.handleSetPollType.bind(this);
        this.props.getAllPolls({});
    }

    componentDidMount() {
        if (this.props.user.username) {
            this.props.getAllPolls({userId: this.props.user.userId});
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.pollData.polls) {
            this.setState({polls: nextProps.pollData.polls});
        }
        if (nextProps.pollData.myPolls) {
            this.setState({myPolls: nextProps.pollData.myPolls});
        }
        if (nextProps.pollData.pollDeleted) {
            alert(nextProps.pollData.pollDeleted);
            this.props.getAllPolls({});
            this.props.getAllPolls({userId: this.props.user.userId});
        }
        if (nextProps.user) {
            this.setState({user: nextProps.user});
        }
    }

    handleSetPollType(e) {
        this.setState({showMyPolls: e.target.value});
    }

    handleDeletePoll(e) {
        this.props.deletePoll(e.target.value);
    }

    render() {
        const {polls, myPolls, user, showMyPolls} = this.state;
        return (
            <div className="container content">
                {user.userId ?
                    <div className="new-poll">
                        <Link to="/newPoll">Add New Poll</Link>
                        <hr/>
                        <div>
                            <button type="button" className="btn btn-sm btn-secondary" value="hide" onClick={this.handleSetPollType}>Show All polls</button>
                            <button type="button" className="btn btn-sm btn-success" value="show" onClick={this.handleSetPollType}>Show My polls</button>
                        </div>
                    </div>
                :
                    ''
                }
                {(showMyPolls === "hide") ?
                    <table className="table table-condensed table-striped">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>View</th>
                            </tr>
                        </thead>
                        <tbody>
                            {polls.map((poll, index) => {
                                return (
                                    <tr key={index} className="poll-item">
                                        <td>
                                            {poll.title}
                                        </td>
                                        <td>
                                            {poll.description}
                                        </td>
                                        <td>
                                            <Link to={"/poll/" + poll._id}>View</Link>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                  :
                  <table className="table table-condensed table-striped">
                      <thead>
                          <tr>
                              <th>Title</th>
                              <th>Description</th>
                              <th>View</th>
                              <th>Delete</th>
                          </tr>
                      </thead>
                      <tbody>
                          {myPolls.map((poll, index) => {
                              return (
                                  <tr key={index} className="poll-item">
                                      <td>
                                          {poll.title}
                                      </td>
                                      <td>
                                          {poll.description}
                                      </td>
                                      <td>
                                          <Link to={"/poll/" + poll._id}>View</Link>
                                      </td>
                                      <td>
                                          <button type="button" className="btn btn-sm btn-danger" value={poll._id} onClick={this.handleDeletePoll}>Delete</button>
                                      </td>
                                  </tr>
                              )
                          })}
                      </tbody>
                  </table>
                }
            </div>
        );
    }
}

Polls.propTypes = {
    getAllPolls: PropTypes.func.isRequired,
    deletePoll: PropTypes.func.isRequired,
    pollData: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

export default Polls;
