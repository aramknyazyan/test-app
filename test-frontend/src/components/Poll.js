import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2'

class Poll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            poll: {},
            user: {},
            vote: {},
            chartData: {},
            optionName: '',
            errors: {}
        };
        this.props.getOnePoll(this.props.match.params.id);
        this.handleDrawChart = this.handleDrawChart.bind(this);
        this.handleRadioChange = this.handleRadioChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if (this.props.user.userId) {
            this.props.getUserVote(this.props.user.userId, this.props.match.params.id);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.poll) {
            this.setState({poll: nextProps.poll});
            this.handleDrawChart(nextProps.poll);
        }
        if (nextProps.user) {
            this.setState({user: nextProps.user});
        }
        if (this.state.vote !== nextProps.vote) {
            this.setState({vote: nextProps.vote});
            this.props.getOnePoll(this.props.match.params.id);
        }
    }

    handleDrawChart(poll) {
        if (!(poll && poll.options && poll.options.length > 0)) {
            return false;
        }
        let labels = [];
        let values = [];
        poll.options.forEach(function(option) {
            labels.push(option.name);
            values.push(option.value);
        });
        const colorSet = {
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)'
        };
        let datasets = [
            {
                label: poll.title,
                fill: false,
                lineTension: 0.1,
                backgroundColor: colorSet.backgroundColor,
                borderColor: colorSet.borderColor,
                borderCapStyle: 'butt',
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: 'miter',
                pointBorderColor: colorSet.borderColor,
                pointBackgroundColor: '#fff',
                pointBorderWidth: 1,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: colorSet.borderColor,
                pointHoverBorderColor: colorSet.pointHoverBorderColor,
                pointHoverBorderWidth: 2,
                pointRadius: 1,
                pointHitRadius: 10,
                data: values
            }
        ];

        const chartData = {
            labels: labels,
            datasets: datasets
        };

        this.setState({chartData: chartData});
    }

    handleRadioChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit(e) {
        e.preventDefault();
        const vote = {
            pollId: this.state.poll._id,
            optionName: this.state.optionName
        }
        this.props.createVote(vote);
    }

    render() {
        const {poll, chartData, user, vote, errors} = this.state;
        return (
            <div className="container content">
                <h4>{poll.title}</h4>
                <p>{poll.description}</p>
                {(!user.userId) ?
                    <div>Please login to vote!</div>
                  : (!vote.optionName) ?
                    <form className="vote-form" onSubmit = {this.handleSubmit}>
                        {poll.options.map((p, index) => {
                            return (
                              <div key={index}>
                                  <input
                                    type="radio"
                                    name="optionName"
                                    onChange={this.handleRadioChange}
                                    value={p.name}
                                  />
                                  <label>{p.name}</label>
                              </div>
                            )
                        })}
                        <div className="form-group">
                            <button type="submit" className="btn btn-primary">
                                Vote
                            </button>
                        </div>
                        {errors.message ?
                            <div className="alert alert-danger">
                                {errors.message}
                            </div>
                        :
                            ''
                        }
                    </form>
                :
                    <div>You have voted for {vote.optionName}</div>
                }
                <hr/>
                <h4>Results</h4>
                {chartData.labels ?
                    <Line data={chartData} />
                :
                    ''
                }
            </div>
        );
    }
}

Poll.propTypes = {
    getOnePoll: PropTypes.func.isRequired,
    createVote: PropTypes.func.isRequired,
    getUserVote: PropTypes.func.isRequired,
    poll: PropTypes.object.isRequired,
    vote: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

export default Poll;
