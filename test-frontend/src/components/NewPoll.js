import React, { Component } from "react";
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/css/style.css';

class NewPoll extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: {},
            title: '',
            description: '',
            options: ['',''],
            errors: {}
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleAddOption = this.handleAddOption.bind(this);
        this.handleRemoveOption = this.handleRemoveOption.bind(this);
    }

    componentDidMount() {
        if (!this.props.user.username) {
            this.props.history.push('/');
        }
    }

    componentWillReceiveProps(nextProps) {
        if(this.state.user !== nextProps.user) {
            this.setState({user: nextProps.user});
        }
        if(this.state.errors !== nextProps.errors) {
            this.setState({errors: nextProps.errors});
        }
    }

    handleInputChange(e) {
        if (e.target.name.indexOf('option') > -1) {
            let index = e.target.name.slice(-1);
            let options = this.state.options;
            options[index] = e.target.value;
            this.setState({options: options});
        } else {
            this.setState({
                [e.target.name]: e.target.value
            });
        }
    }

    handleAddOption(e) {
        let options = this.state.options;
        options.push('');
        this.setState({options: options});
    }

    handleRemoveOption(e) {
        let options = this.state.options;
        options.pop();
        this.setState({options: options});
    }

    handleSubmit(e) {
        e.preventDefault();
        const poll = {
            title: this.state.title,
            description: this.state.description,
            options: this.state.options
        }
        this.props.createPoll(poll, this.props.history);
    }

    render() {
        const {title, description, options, errors} = this.state;
        return (
            <div>
                <div className="container content">
                    <form className="poll-form" onSubmit={ this.handleSubmit }>
                        <h4>Create New Poll</h4>
                        <div className="form-group">
                            <input
                              type="text"
                              placeholder="Title"
                              className={errors.title ? 'form-control error-input' : 'form-control'}
                              name="title"
                              onChange={this.handleInputChange}
                              value={title}
                            />
                            {errors.title ? <div className="error-text">{errors.title}</div> : ''}
                        </div>
                        <div className="form-group">
                            <input
                              type="text"
                              placeholder="Description"
                              className={errors.description ? 'form-control error-input' : 'form-control'}
                              name="description"
                              onChange={this.handleInputChange}
                              value={description}
                            />
                            {errors.description ?  <div className="error-text">{errors.description}</div> : ''}
                        </div>
                        {options.map((option, index) => {
                            return (
                                <div className="form-group" key={index}>
                                    <input
                                      type="text"
                                      placeholder={"Option" + (index + 1)}
                                      className={errors["option" + index] ? 'form-control error-input' : 'form-control'}
                                      name={"option" + index}
                                      onChange={this.handleInputChange}
                                      value={options[index]}
                                    />
                                    {errors["option" + index] ? <div className="error-text">{errors["option" + index]}</div> : ''}
                                </div>
                            )
                        })}
                        {(options.length < 9) ?
                            <div className="form-group">
                                <button type="button" onClick={this.handleAddOption} className="btn btn-block btn-success">
                                    + Add new option
                                </button>
                            </div>
                        :
                            ''
                        }
                        {(options.length > 2) ?
                            <div className="form-group">
                                <button type="button" onClick={this.handleRemoveOption} className="btn btn-block btn-danger">
                                    - Remove option
                                </button>
                            </div>
                        :
                            ''
                        }
                        <div className="form-group">
                            <button type="submit" className="btn btn-block btn-primary">
                                Submit
                            </button>
                        </div>
                        {errors.message ?
                            <div className="alert alert-danger">
                                {errors.message}
                            </div>
                        :
                            ''
                        }
                    </form>
                </div>
            </div>
        );
    }
}

NewPoll.propTypes = {
    createPoll: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

export default NewPoll;
