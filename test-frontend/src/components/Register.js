import React, { Component } from "react";
import PropTypes from 'prop-types';
import 'bootstrap/dist/css/bootstrap.css';
import '../assets/css/style.css';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            repeatPassword: '',
            errors: {}
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if (this.props.user.username) {
            this.props.history.push('/');
        }
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.user.username) {
            this.props.history.push('/')
        }
        if(nextProps.errors) {
            this.setState({errors: nextProps.errors});
        }
    }

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        const user = {
            username: this.state.username,
            password: this.state.password,
            repeatPassword: this.state.repeatPassword
        }
        this.props.register(user, this.props.history);
    }

    render() {
        const {username, password, repeatPassword, errors} = this.state;
        return (
          <div className="container content">
              <form className="user-form" onSubmit={ this.handleSubmit }>
                  <h4>Login</h4>
                  <div className="form-group">
                      <input
                        type="text"
                        placeholder="Username"
                        className={errors.username ? 'form-control error-input' : 'form-control'}
                        name="username"
                        onChange={this.handleInputChange}
                        value={username}
                      />
                      {errors.username ? <div className="error-text">{errors.username}</div> : ''}
                  </div>
                  <div className="form-group">
                      <input
                      type="password"
                      placeholder="Password"
                      className={errors.password ? 'form-control error-input' : 'form-control'}
                      name="password"
                      onChange={this.handleInputChange}
                      value={password}
                      />
                      {errors.password ? <div className="error-text">{errors.password}</div> : ''}
                  </div>
                  <div className="form-group">
                      <input
                      type="password"
                      placeholder="Repeat Password"
                      className={errors.repeatPassword ? 'form-control error-input' : 'form-control'}
                      name="repeatPassword"
                      onChange={this.handleInputChange}
                      value={repeatPassword}
                      />
                      {errors.repeatPassword ? <div className="error-text">{errors.repeatPassword}</div> : ''}
                  </div>
                  <div className="form-group">
                      <button type="submit" className="btn btn-block btn-primary">
                          Submit
                      </button>
                  </div>
                  {errors.message ?
                      <div className="alert alert-danger">
                          {errors.message}
                      </div>
                  :
                      ''
                  }
              </form>
          </div>
        );
    }
}

Register.propTypes = {
    register: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
}

export default Register;
