import { SET_POLL } from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {
        case SET_POLL:
            return action.payload
        default:
            return state;
    }
}
