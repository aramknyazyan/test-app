import { SET_VOTE } from '../actions/types';

export default function(state = {}, action) {
    switch(action.type) {
        case SET_VOTE:         
            return action.payload
        default:
            return state;
    }
}
