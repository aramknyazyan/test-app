import { SET_POLLS, SET_MY_POLLS, SET_POLL_DELETED } from '../actions/types';

const initialState = {
    polls: [],
    myPolls: [],
    pollDeleted: ''
}

export default function(state = initialState, action) {
    switch(action.type) {
        case SET_POLLS:
            return {
                ...state,
                polls: action.payload,
                pollDeleted: ''
            }
        case SET_MY_POLLS:
            return {
                ...state,
                myPolls: action.payload,
                pollDeleted: ''
            }
        case SET_POLL_DELETED:
            return {
                ...state,
                pollDeleted: action.payload
            }
        default:
            return state;
    }
}
