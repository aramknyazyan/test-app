import { combineReducers } from 'redux';
import errorReducer from './errorReducer';
import userReducer from './userReducer';
import pollReducer from './pollReducer';
import pollsReducer from './pollsReducer';
import newPollReducer from './newPollReducer';
import voteReducer from './voteReducer';

export default combineReducers({
    errors: errorReducer,
    user: userReducer,
    poll: pollReducer,
    pollData: pollsReducer,
    newPoll: newPollReducer,
    vote: voteReducer
});
