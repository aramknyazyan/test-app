import { GET_ERRORS } from '../actions/types';
import { GET_VALIDATION_ERRORS } from '../actions/types';

export default function(state = {}, action ) {
    switch(action.type) {
        case GET_VALIDATION_ERRORS:
            return action.payload;
        case GET_ERRORS:
            let payload = action.payload.err;
            if (action.payload.code === 500) {
                payload = {message: "Something went wrong!"};
            }
            return payload;
        default:
            return state;
    }
}
