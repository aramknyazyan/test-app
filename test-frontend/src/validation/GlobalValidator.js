import v from 'validator';

const nameMin = 3;
const nameMax = 24;
const passwordMin = 6;
const passwordMax = 16;
const textMin = 1;
const textMax = 60;
const optionMin = 1;
const optionMax = 24;

const messages = {
    isRequired: "The field is required!",
    forbiddenSymbols: "Some symbols are forbidden!",
};

const GlobalValidator = {
    isRequired(param) {
        if (!param) {
            return messages.isRequired;
        }
        return null;
    },
    isName(param, isRequired) {
        if (isRequired && !param) {
            return messages.isRequired;
        }
        if (!isRequired && !param) {
            return null;
        }
        if (v.matches(String(param), /[`~!@#$%^&*()=+|{}/[\];:'",<.>? ]/)) { //actually everything apart the regex symbols
            return messages.forbiddenSymbols;
        }
        if (!v.isLength(String(param), {min: nameMin, max: nameMax})) {
            return `Field must be between ${nameMin} and ${nameMax} symbols!`;
        }

        return null;
    },
    isPassword(param, isRequired) {
        if (isRequired && !param) {
            return messages.isRequired;
        }
        if (!isRequired && !param) {
            return null;
        }
        if (v.matches(String(param), /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])+$/)) {
            return messages.forbiddenSymbols;
        }
        if (!v.isLength(String(param), {min: passwordMin, max: passwordMax})) {
            return `Field must be between ${passwordMin} and ${passwordMax} symbols!`;
        }
        return null;
    },
    isText(param, isRequired) {
        if (isRequired && !param) {
            return messages.isRequired;
        }
        if (!isRequired && !param) {
            return null;
        }
        if (!v.isLength(String(param), {min: textMin, max: textMax})) {
            return `Field must be between ${textMin} and ${textMax} symbols!`;
        }
        if (!v.matches(String(param), /^[a-zA-Z0-9_-\s!?,.]+$/)) {
            return messages.forbiddenSymbols;
        }

        return null;
    },
    isOption(param, isRequired) {
        if (isRequired && !param) {
            return messages.isRequired;
        }
        if (!isRequired && !param) {
            return null;
        }
        if (!v.isLength(String(param), {min: optionMin, max: optionMax})) {
            return `Field must be between ${optionMin} and ${optionMax} symbols!`;
        }
        if (!v.matches(String(param), /^[a-zA-Z0-9_-\s!?,.]+$/)) {
            return messages.forbiddenSymbols;
        }

        return null;
    }
}

export default GlobalValidator;
