import GlobalValidator from './GlobalValidator';

const ValidationService = {
    validateAuthentication(params) {
        let errors = {};
        let errorUsername = GlobalValidator.isName(params.username, true);
        if (errorUsername) {
            errors.username = errorUsername;
        }
        let errorPassword = GlobalValidator.isPassword(params.password, true);
        if (errorPassword) {
            errors.password = errorPassword;
        }

        let success = Object.keys(errors).length;
        return {success: !success, errors: errors};
    },
    validateRegistration(params) {
        let errors = {};
        let errorUsername = GlobalValidator.isName(params.username, true);
        if (errorUsername) {
            errors.username = errorUsername;
        }
        let errorPassword = GlobalValidator.isPassword(params.password, true);
        if (errorPassword) {
            errors.password = errorPassword;
        }
        if (params.password !== params.repeatPassword) {
            errors.repeatPassword = "Mismatch with password!";
        }

        let success = Object.keys(errors).length;
        return {success: !success, errors: errors};
    },
    validateCreatePoll(params) {        
        let errors = {};
        let errorTitle = GlobalValidator.isName(params.title, true);
        if (errorTitle) {
            errors.title = errorTitle;
        }
        let errorDescription = GlobalValidator.isText(params.description, true);
        if (errorDescription) {
            errors.description = errorDescription;
        }
        params.options.forEach((name, index) => {
            let errorOption = GlobalValidator.isOption(name, true);
            if (errorOption) {
                let optionVar = 'option' + index;
                errors[optionVar] = errorOption;
            }
        });

        let success = Object.keys(errors).length;
        return {success: !success, errors: errors};
    }
};

export default ValidationService;
