import React, { Component } from 'react';
import Routes from './routes';
import { Provider } from 'react-redux';
import store from './store';
import { setCurrentUser, logout } from './actions/actions';
import setToken from './setToken';

if (localStorage.getItem('token')) {
  const token = localStorage.getItem('token');
  const userId = localStorage.getItem('userId');
  const username = localStorage.getItem('username');
  const exp = localStorage.getItem('exp');
  const currentTime = Date.now() / 1000;
  if (exp < currentTime) {
      store.dispatch(logout(false));
      window.location.href = '/login';
  } else {
      setToken(token);
      store.dispatch(setCurrentUser({userId: userId, username: username}));
  }
}

class App extends Component {
  render() {
    return (
      <Provider store = {store}>
          <Routes/>
      </Provider>
    );
  }
}

export default App;
