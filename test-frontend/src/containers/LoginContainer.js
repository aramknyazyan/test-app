import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { login } from '../actions/actions';
import Login from '../components/Login';

const mapStateToProps = state => ({
    user: state.user,
    errors: state.errors
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        login
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
