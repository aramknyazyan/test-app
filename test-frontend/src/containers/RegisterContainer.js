import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { register } from '../actions/actions';
import Register from '../components/Register';

const mapStateToProps = state => ({
    user: state.user,
    errors: state.errors
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        register
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Register);
