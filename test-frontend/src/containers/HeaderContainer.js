import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout } from '../actions/actions';
import Header from '../components/Header';

const mapStateToProps = state => ({
    user: state.user
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        logout
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
