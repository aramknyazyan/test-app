import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createPoll } from '../actions/actions';
import NewPoll from '../components/NewPoll';

const mapStateToProps = state => ({
    user: state.user,
    errors: state.errors
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        createPoll
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(NewPoll);
