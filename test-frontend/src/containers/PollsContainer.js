import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getAllPolls, deletePoll } from '../actions/actions';
import Polls from '../components/Polls';

const mapStateToProps = state => ({
    pollData: state.pollData,
    user: state.user,
    errors: state.errors
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        getAllPolls,
        deletePoll
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Polls);
