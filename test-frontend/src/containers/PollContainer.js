import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getOnePoll, createVote, getUserVote } from '../actions/actions';
import Poll from '../components/Poll';

const mapStateToProps = state => ({
    poll: state.poll,
    vote: state.vote,
    user: state.user,
    errors: state.errors
});

const mapDispatchToProps = dispatch => (
    bindActionCreators({
        getOnePoll,
        createVote,
        getUserVote
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Poll);
