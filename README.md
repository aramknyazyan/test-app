## Test web application


This is a test web project which uses React.js as front-end application and Node.js + MongoDB for backend API.
In application you can:
 - Register/Login a user account
 - View available polls and charts of votes for each poll
 - Create your own poll with any polling options (if you are authenticated)
 - View the list of polls created by you and delete any of those (again if you are authenticated)

In order to start the application, you need to have docker and docker-compose installed on your machine.
Or if you already have Node.js and MongoDB installed on your machine, you can just start the react app and node server in test-frontend and test-backend folders (you can find the descriptions in README files in test-frontend and test-backend folders).

To start the application by docker in root of project run

### `docker-compose build`

It will build 3 docker containers, one for MongoDB, one for Node.js, and one for React.js, and also will join those to the same network.
After successful build you can run:

### `docker-compose up`
or with `-d` flag to run in background mode

It will start the docker containers and will bind your machine ports to the docker ports.
Open [http://localhost:3000](http://localhost:3000) to view the application it in the browser.
If you are running the application in virtual machine you need to change the apiUrl in `test-frontend/src/configs/config.json` from localhost to virtual machine IP.
The same change needs to be done in `test-backend/app/config/config.js` in order to test graphQL API methods.
